#
# This Dockerfile creates an image that can be used
#   to inspect Rancher NFS partitions as well as transfer
#   file (via `scp`) to an from those partitions.
#

# The following instruction is equal to the line below, but uses gitlabs Dependency Proxy
#    FROM alpine:3.16.3
FROM gitlab.com:443/nest.lbl.gov/dependency_proxy/containers/alpine:3.16.3


LABEL maintainer="Simon Patton <sjpatton@lbl.gov, Keith Beattie <ksbeattie@lbl.gov>"

# Stop error messages in container
COPY inittab /etc/inittab

RUN apk update && \
    apk add --no-cache \
            bash \
            openssh-client \
	        emacs \
	        curl \
	        netcat-openbsd \
	        rsync \
	        git \
	        util-linux \
	        coreutils \
	        findutils \
	        iproute2 \
	        bind-tools

ENTRYPOINT ["/sbin/init"]

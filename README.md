# What is `app-inspector`? #

The `app-inspector` image is designed provide utilities to other images that are group of services within an orchestration unit. This unit may be a [kubernetes namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/), [Docker swarm](https://docs.docker.com/engine/swarm/), or similar orchestration systems. In many cases those other images are "official" images and therefore only have install those utilities needed by that service. However there are often cases where access to other utilities is required. Obvious examples are editors and networking tools.

The typical usage of the `app-inspector` image is to mount volumes that are shared with one or more other images within the orchestration unit but not visible outside that unit. The contents of those volumes can then manipulated are necessary by running a shell in the `app-inspector` container and using the appropriate utilities.

Currently the `app-inspector` image contains the following utilities.

* `bash`: This provides a shell in whic to use the utilities lsited below.
* `ssh` client: This enables files external to the orchestration unit to be copied on the internal volumes.
* `emacs`: This enables files, particularly configuration files on internal volumes, to edited - especially as most "official" images do not include an editor.
* `curl`: This enables Web resources to be copies on the internal volumes.
* `netcat`: This enables to network within the orchestration unit  to be explored and tested.
* `rsync`: In conjunction with the `ssh` client, this enables files already existing on internal volumes to be updated to a latest external versions.
* `git`: This enables git repositories to be loaded onto internal volumes. This is especially useful in the cases where configuration or content files are stored in a `git` repository.
* `util-linux`: Random collection of Linux utilities.
* `corutials`: The basic file, shell and text manipulation utilities.
* `findutils`: GNU utilities for finding files.
* `grep`: Searches input files for lines containing a match to a specified pattern.
* `iproute2`: IP Routing Utilities.
* `bind-tools`: The ISC DNS tools.


## Creating a new `app-inspector` image ##

The [gitlab.com repository](https://gitlab.com/nest.lbl.gov/images/app-inspector) for this image is configured to build a new image whenever a tag of the form "XX.YY.ZZ.aaa" is created. This image will be then be available from its [gitlab container registry](https://gitlab.com/nest.lbl.gov/images/app-inspector/container_registry).

Of course this does not mean people are not free to clone the repository and do their own `docker build` with the contents.
